#!/usr/bin/env python3
import mido
import sys
from time import sleep

mido.set_backend('mido.backends.portmidi')


def test_midi_send_out(note):
    msg = mido.Message('note_on', note=note, velocity=127)
    if 'MidiSport 1x1 MIDI 1' in mido.get_output_names():
        outportname = 'MidiSport 1x1 MIDI 1'
    else:
        print('NO MidiSport FOUND')
        sys.exit(0)
    with mido.open_output(outportname) as outport:
        print(outport)
        i = 0
        while i < 10:
            i += 1
            print("MIDI OUT >>> Outport: {}; Msg: {}".format(outport, msg))
            outport.send(msg)
            sleep(2)

test_midi_send_out(10)

