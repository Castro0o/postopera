#!/usr/bin/env python3
import subprocess
import shlex
import os
from argparse import ArgumentParser
from csv import DictReader
from time import sleep
import mido
from log import logger
from light_sequence import run_light_sequence

parser = ArgumentParser(description='Installation master loop')
parser.add_argument('-r', '--row', type=int, default=0,
                    help='start row number')
args = parser.parse_args()

mido.set_backend('mido.backends.portmidi')

_dir = os.path.dirname(os.path.abspath(__file__))
csv = _dir + '/timeline.csv'


def ssh_cmd(pi, cmd):
    ssh = "ssh {pi} '{cmd}'".format(pi=pi, cmd=cmd)
    logger.debug("ssh >>> {}".format(ssh))
    subprocess.Popen(shlex.split(ssh))


def timestr2sec(timestr):
    """
    converts time duration string: hh:mm:ss to seconds (int)
    """
    t_list = timestr.split(":")
    t_list = [float(unit) for unit in t_list]
    t_seconds = int((t_list[0] * 60 * 60) + (t_list[1] * 60) + t_list[2])
    return t_seconds


#  ----- Start installation sequence  ----- #


logger.debug("\n\n")
logger.debug("--------------> Starting Loop <-----------")

logger.debug(" oooooooo !lights out! oooooooo")
run_light_sequence('all_0')

while True:
    with open(csv, newline='') as csvfile:
        csvreader = DictReader(csvfile)
        for row in csvreader:
            if int(row['Order']) >= args.row:
                row_dur = timestr2sec(row['Duration'])
                logger.debug(
                    "CSV row: {} {} {} {} {}".format(row['Order'],
                                                     row['Pi'],
                                                     row['Action'],
                                                     row['Command'],
                                                     row['Duration'])
                )
                if (row['Command'].split(' '))[0] in ['aplay', 'ecasound']:
                    ssh_cmd(pi=row['Pi'], cmd=row['Command'])
                elif (row['Command']).startswith('l'):
                    run_light_sequence(row['Command'])
                sleep(row_dur)
