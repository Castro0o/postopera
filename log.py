import logging
import os
from logging.handlers import RotatingFileHandler


def log(_dir, filename):
    """
    Simple logger object.
    Logs Installation loop.py actions
    """
    logger = logging.getLogger('InstaLLation Loop Log')
    log_format = logging.Formatter('%(asctime)-15s %(message)s')
    log_path = _dir + filename  # prepend "/" to filename
    log_handler = RotatingFileHandler(filename=log_path,
                                      maxBytes=512000,
                                      backupCount=2)  # 512Kb log file
    log_handler.setFormatter(log_format)
    logger.addHandler(log_handler)
    logger.setLevel(logging.DEBUG)
    return logger


_dir = os.path.dirname(os.path.abspath(__file__))
logger = log(_dir, "/installation.log")  # create logger
