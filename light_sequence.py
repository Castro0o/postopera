#!/usr/bin/env python3
import mido
from time import sleep
from log import logger


mido.set_backend('mido.backends.portmidi')


def midi_send_out(note):
    msg = mido.Message('note_on', note=note, velocity=127)
    if 'MidiSport 1x1 MIDI 1' in mido.get_output_names():
        outportname = 'MidiSport 1x1 MIDI 1'
    else:
        outportname = 'Midi Through Port-0'
    with mido.open_output(outportname) as outport:
        # logger.debug(
        # "MIDI OUT >>> Outport: {}; Msg: {}".format(outport, msg))
        outport.send(msg)


def run_light_sequence(sequence_key):
    sequence = light_sequences[sequence_key]
    for step in sequence:
        midi_send_out(note=step['MIDI'])
        logger.debug("light >>> {}".format(step['description']))
        sleep(10)


light_scenes = {
    'l1_fadein': {'bank': 1,
                  'MIDI': 0,
                  'description': 'fadein on light1'
                  },
    'l1_fadeout': {'bank': 2,
                   'MIDI': 8,
                   'description': 'fadeout on light1'
                   },
    'l1_100': {'bank': 10,
               'MIDI': 72,
               'description': 'constant 100% on light1'
               },
    'l2_fadein': {'bank': 3,
                  'MIDI': 16,
                  'description': 'fadein on light2'
                  },
    'l2_fadeout': {'bank': 4,
                   'MIDI': 24,
                   'description': 'fadeout on light2'
                   },
    'l2_100': {'bank': 11,
               'MIDI': 80,
               'description': 'constant 100% on light2'
               },
    'l3_fadein': {'bank': 5,
                  'MIDI': 32,
                  'description': 'fadein on light3'
                  },
    'l3_fadeout': {'bank': 6,
                   'MIDI': 49,
                   'description': 'fadeout on light3'
                   },
    'l3_100': {'bank': 12,
               'MIDI': 88,
               'description': 'constant 100% on light3'
               },

    'all_0': {'bank': 15,
              'MIDI': 112,
              'description': 'constant 0% on all lights'
              }
}


light_sequences = {
    'l1_fadein': [light_scenes['l1_fadein'], light_scenes['l1_100']],
    'l1_fadeout': [light_scenes['l1_fadeout'], light_scenes['all_0']],
    'l2_fadein': [light_scenes['l2_fadein'], light_scenes['l2_100']],
    'l2_fadeout': [light_scenes['l2_fadeout'], light_scenes['all_0']],
    'l3_fadein': [light_scenes['l3_fadein'], light_scenes['l3_100']],
    'l3_fadeout': [light_scenes['l3_fadeout'], light_scenes['all_0']],
    'all_0': [light_scenes['all_0']],

}

if __name__ == '__main__':
    run_light_sequence(light_sequences['l1_fadein'])
    sleep(5)
    run_light_sequence(light_sequences['l1_fadeout'])
    sleep(5)
    run_light_sequence(light_sequences['l2_fadein'])
    sleep(5)
    run_light_sequence(light_sequences['l2_fadeout'])
    sleep(5)
    run_light_sequence(light_sequences['l3_fadein'])
    sleep(5)
    run_light_sequence(light_sequences['l3_fadeout'])
